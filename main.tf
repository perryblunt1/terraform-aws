terraform {
  required_providers {
      aws = {
          source = "hashicorp/aws"
          version = "~>3.0"
      }
  }
}

provider "aws" {
    region = "us-east-1"
  
}

#Create a vpc

resource "aws_vpc" "Lab-VPC" {
    cidr_block = var.cidr_block[0]


    tags = {
    Name = "MyLab-VPC"

}
  
}

#Create Subnet (Public)

resource "aws_subnet" "Lab-Subnet-1" {
  vpc_id     = aws_vpc.Lab-VPC.id
  cidr_block = var.cidr_block[1]

  tags = {
    Name = "Lab-Subnet-1"
  }
}

#Create internet Gateway

resource "aws_internet_gateway" "Lab-IGW" {
  vpc_id = aws_vpc.Lab-VPC.id

  tags = {
    Name = "Lab-IGW"
  }
}

#Creating security Groups

resource "aws_security_group" "Lab-sg" {
  name        = "Lab Security Group"
  description = "Allow inbound and outbound traffic"
  vpc_id      = aws_vpc.Lab-VPC.id

  dynamic ingress {
      iterator = port
      for_each = var.ports
        content {
            from_port = port.value
            to_port = port.value
            protocol = "tcp"
            cidr_blocks = ["0.0.0.0/0"]
        }
  }   

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "Lab-allow_ssh"
  }
}

# Create route table and association

resource "aws_route_table" "Lab-rt" {
    vpc_id = aws_vpc.Lab-VPC.id

    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_internet_gateway.Lab-IGW.id
    }

    tags = {
       Name  = "Lab-Routetable"
    }
  
}

resource "aws_route_table_association" "Lab-Assn" {
    subnet_id = aws_subnet.Lab-Subnet-1.id
    route_table_id = aws_route_table.Lab-rt.id
  
}

# Create EC2 instance

resource "aws_instance" "jenkins" {
  ami           = var.ami
  instance_type = var.instance_type
  key_name = "EC2"
  vpc_security_group_ids = [aws_security_group.Lab-sg.id]
  subnet_id = aws_subnet.Lab-Subnet-1.id
  associate_public_ip_address = true
  

  tags = {
    Name = "Jenkins-Server"
  }
}

# Create EC2 instance
resource "aws_instance" "Nexus" {
  ami           = var.ami2
  instance_type = var.instance_type2
  key_name = "EC2"
  vpc_security_group_ids = [aws_security_group.Lab-sg.id]
  subnet_id = aws_subnet.Lab-Subnet-1.id
  associate_public_ip_address = true
  

  tags = {
    Name = "Nexus"
  }
}